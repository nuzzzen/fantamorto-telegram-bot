import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "./vendor"))

import logging
import asyncio
import requests
import json
from telegram import Update, ReplyKeyboardMarkup
from telegram.ext import \
    ApplicationBuilder, \
    ContextTypes, \
    CommandHandler

import login
import scraper

STATUS_OK = {"statusCode": 200,
             'headers': {'Content-Type': 'application/json'},
             'body': json.dumps('ok')
             }
STATUS_ERROR = {"statusCode ": 500,
                'body': json.dumps('Oops, something went wrong!')
                }
TELEGRAM_TOKEN = os.environ["TELEGRAM_TOKEN"]

### Setup Logging ###
logger = logging.getLogger()
if logger.handlers:
    for handler in logger.handlers:
        logger.removeHandler(handler)
logging.basicConfig(level=logging.INFO)

### Setup telegram bot ###
application = ApplicationBuilder().token(token=TELEGRAM_TOKEN).build()


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    # TODO add help information (commands and functions the user should expect from the bot)
    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text=f'Ciao @{update.message.from_user.username}!')


async def check_leaderboard(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """ prints the teams leaderboard and set the menu state """
    # TODO insert emoji in text answer
    msg = await update.message.reply_text(reply_to_message_id=update.message.message_id,
                                          text='Recupero la classifica . . .')

    html = login.login()
    if html:
        reply_text = scraper.scrape_leaderboard(html)
        await context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=msg.message_id,
                                            text='```\n' + reply_text + '```', parse_mode='MarkdownV2')
        return STATUS_OK
    else:
        await update.message.reply_text(reply_to_message_id=update.message.message_id,
                                        text='Scraping went wrong!')
        return STATUS_ERROR


async def choose_team_to_fetch(update: Update, context: ContextTypes.DEFAULT_TYPE):

    html = login.login()
    if html:
        reply_text = scraper.get_team_names_from_html(html)
        keyboard_markup = ReplyKeyboardMarkup(
            reply_text,
            resize_keyboard=True,
            one_time_keyboard=True,
            selective=True,
        )

        await update.message.reply_text(reply_to_message_id=update.message.message_id,
                                        text=f'@{update.message.from_user.username} Scegli una squadra dalla lista:',
                                        reply_markup=keyboard_markup, )

        return STATUS_OK

    else:
        await update.message.reply_text(reply_to_message_id=update.message.message_id,
                                        text='Cannot obtain teams!')
        return STATUS_ERROR


async def fetch_team_stats(update: Update, context: ContextTypes.DEFAULT_TYPE):
    team_to_fetch = " ".join(context.args)
    logger.info(f'looking for: {team_to_fetch} team')
    html = login.login()
    if html:
        teams_list = scraper.scrape_teams(html)
        for team in teams_list:
            logger.info(f'checking team "{team["team_name"]}"')
            if team["team_name"] == team_to_fetch:
                await update.message.reply_text(reply_to_message_id=update.message.message_id,
                                                text=f'@{update.message.from_user.username} '
                                                     f'Recupero le statistiche della squadra "{team["team_name"]}"...')

                reply_text = team['team_players']
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text='```\n' + reply_text + '```', parse_mode='MarkdownV2')
                return STATUS_OK

        await update.message.reply_text(reply_to_message_id=update.message.message_id,
                                        text=f'Cannot find {team_to_fetch} team!')
        return STATUS_ERROR


def lambda_handler(event, context):
    return asyncio.get_event_loop().run_until_complete(main(event, context))


async def add_command_handlers():
    help_command_handler = CommandHandler('help', help)
    application.add_handler(help_command_handler)

    check_leaderboard_command_handler = CommandHandler('checka', check_leaderboard)
    application.add_handler(check_leaderboard_command_handler)

    fetch_team_command_handler = CommandHandler('teams', choose_team_to_fetch)
    application.add_handler(fetch_team_command_handler)

    fetch_team_stats_command_handler = CommandHandler('fetch', fetch_team_stats)
    application.add_handler(fetch_team_stats_command_handler)


async def main(event, context):
    logger.info('Event: {}'.format(event))

    await add_command_handlers()

    try:
        await application.initialize()
        await application.process_update(
            Update.de_json(json.loads(event['body']), application.bot)
        )

        return STATUS_OK

    except Exception as exc:
        logger.info(exc)
        return STATUS_ERROR
