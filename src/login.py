import os
import requests
import logging

LOGIN_URL = 'https://fantamorto.org/wp-login.php'
FANTAMORTO_USER = os.environ["FANTAMORTO_USER"]
FANTAMORTO_PASS = os.environ["FANTAMORTO_PWD"]


logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)

# TODO Remove if not really needed
# Not needed at the moment
# FANTAMORTO_LOGIN_URL = 'https://www.fantamorto.org/accedi'
# LEGA_URL = 'https://www.fantamorto.org/la-mia-lega'


def login():
    logger.info("Logging in...")
    session = requests.Session()
    login_data = {
        'log': FANTAMORTO_USER,
        'pwd': FANTAMORTO_PASS,
        'wp-submit': 'Login',
        'redirect_to': '/la-mia-lega'
    }
    response = session.post(LOGIN_URL, data=login_data)

    if 'la-mia-lega' in response.url and response.status_code == 200:
        logger.info("Login succeded.")
        return response.text
    else:
        logger.info("Login failed.")
        return None
