import logging
import bs4
from tabulate import tabulate

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def get_team_names_from_html(html):
    """get html text and return a list of team names
    :param html: html text of fantamorto.org/la-mia-lega web page(str)
    :return list of team names(list)
    """

    try:
        soup = bs4.BeautifulSoup(html, 'html.parser')
        leaderboard = soup.find("ol", {"class": "classifica_sidebar"})

    except Exception as exc:
        logger.info(exc)
        return "Something went wrong while scraping the teams!"

    teams = leaderboard.findAll('li')
    squadre = []
    for team in teams:
        squadre.append(['/fetch ' + team.find('a', class_='rollsq').getText()])
    logger.info("Got the teams!\n")
    return squadre


def scrape_leaderboard(html):
    """get html text and return a parsed text with users leaderboards
    :param html: hmtl text of fantamorto.org/la-mia-lega web page(str)
    :return parsed text message of users leaderboard(str)
    """
    soup = bs4.BeautifulSoup(html, 'html.parser')
    leaderboard_list = []
    leaderboard = soup.find("ol", {"class": "classifica_sidebar"})
    if leaderboard:
        teams = leaderboard.findAll('li')
        for team in teams:
            team_dict = {'name': team.find('a', class_='rollsq').getText(),
                         'points': team.find('span', class_='punteggio').getText(),
                         'deaths': team.find('span', class_='morticl').getText()}
            leaderboard_list.append(team_dict)

        headers = ["Squadra", "P", "M"]
        rows = [[entry['name'], entry['points'], entry['deaths']] for entry in leaderboard_list]
        table = tabulate(rows, headers=headers, tablefmt='pretty', colalign=('left', 'center', 'center'),
                         maxcolwidths=[16, 3, 2])

        return table


def scrape_teams(html):
    soup = bs4.BeautifulSoup(html, 'html.parser')
    teams_list = []
    leaderboard = soup.find("div", {"data-name": "listasquadrelega"})
    for team in leaderboard.find_all("div", {"class": "squadra_lista col-md-4 col-lg-3 col-6"}):
        team_list = [entry for entry in team.getText().split('\n') if entry != '']
        tmp_dict = {}
        reply_text = ''
        # get team name and owner
        numsq = team.find("span", {"class": "numsq"}).getText()
        team_name, owner_name = team_list[0].replace(numsq, '').split('by ')
        for team_member in team_list[1:]:
            reply_text += str(team_member) + '\n'

        tmp_dict['owner_name'] = owner_name
        tmp_dict['team_name'] = team_name
        tmp_dict['team_players'] = reply_text
        teams_list.append(tmp_dict)
    return teams_list
