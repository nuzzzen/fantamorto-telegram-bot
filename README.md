# Fantamorto Telegram Bot

This simple bot is useful if you have a private league on [fantamorto.org](https://fantamorto.org). It has two main functions:
- The first one is designed to return the league leaderboard.
- The second returns a team of your choice along with the players and their points. (currently work in progress -  we're trying to figure out what could be the best way to do this).

The bot is deployed using the Serverless framework to AWS Lambda.

---

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/nuzzzen/fantamorto-telegram-bot?label=pipeline&style=for-the-badge&logo=gitlab)

![GitLab Release (latest by date)](https://img.shields.io/gitlab/v/release/nuzzzen/fantamorto-telegram-bot?style=for-the-badge&logo=gitlab)

![GitLab](https://img.shields.io/gitlab/license/nuzzzen/fantamorto-telegram-bot?style=for-the-badge)


## Docker 🐳
> ⚠️ **WARNING** ⚠️: This function is currently not working due to significant refactoring that was done to make the bot operate in AWS Lambda. 
> 

> ~~Place your Telegram Bot Token and [fantamorto.org](fantamorto.org) credentials into `keys.py` before building~~.

```bash
git clone https://gitlab.com/nuzzzen/fantamorto-telegram-bot.git \
    && cd fantamorto-telegram-bot

docker build -t fantamorto-telegram-bot .

docker run fantamorto-telegram-bot
```

<details>
  <summary><b>Useful dev links</b></summary>  

- https://smyachenkov.com/posts/aws-lambda-telegram/
- https://github.com/Andrii-D/serverless-telegram-bot --> related to the one above
</details>


## Disclaimer

> The web scraping tool and its associated software have been designed and developed solely for the purpose of retrieving publicly available data from websites. It is to be noted that the use of this tool is strictly at the user's own risk and discretion.
The makers of this tool do not condone the misuse of the tool by any user for activities that are in violation of the website owner’s stated policies, terms and conditions, or any relevant laws and regulations.

> Furthermore, it is important to understand that the use of this tool may result in the display of warning messages or other notifications from website owners. While we have taken reasonable measures to ensure that our tool operates within ethical boundaries and does not cause harm to websites or their owners, it is the responsibility of the user to take steps to comply with such warnings or notifications.

> Therefore, the makers of this tool disclaim any responsibility or liability for any misuse, harm or legal actions arising from its use. The user accepts full responsibility for any actions taken with the use of this tool.

> By using this tool, the user acknowledges and agrees to this disclaimer.

--- 

## Contacts

For any question or concern, please open an Issue describing what you need.
