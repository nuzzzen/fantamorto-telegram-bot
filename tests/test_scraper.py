import subprocess

def test_scraper():
    result = subprocess.run(['python', 'src/scraper.py'], capture_output=True)
    assert result.returncode == 0, f"scraper.py failed with output: {result.stdout.decode('utf-8')}"
