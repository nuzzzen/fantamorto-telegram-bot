FROM python:3.7-alpine3.16

WORKDIR /app

RUN apk update --no-cache \
    && adduser -D fantamorto-bot \
    && chown -R fantamorto-bot:fantamorto-bot /app


USER fantamorto-bot

ENV PATH=/home/fantamorto-bot/.local/bin:$PATH

COPY requirements.txt /app/requirements.txt 
COPY src/ /app/
RUN /usr/local/bin/python -m pip install --upgrade pip \
    && pip install --no-warn-script-location --user -r requirements.txt

CMD ["/usr/local/bin/python", "telegram_bot.py"]

